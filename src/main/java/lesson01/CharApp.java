package lesson01;

public class CharApp {
  public static void main(String[] args) {
    int c = 'A' + 1;
    int range = 'Z' - 'A' + 1;
    char x = (char) c;
//    System.out.println(range);
    String s1 = "Hello";  // ASCII
    String s2 = "Привет"; // UTF
    System.out.println(s1.length()); // 5
    System.out.println(s2.length()); // 6
    byte[] bytes1 = s1.getBytes();
    byte[] bytes2 = s2.getBytes();
    System.out.println(bytes1.length); // 5
    System.out.println(bytes2.length); // 12
  }
}
