package lesson01;

public class ControlStructApp {

  public static void main(String[] args) {
    // if conditional
    int a = 5;
    int b = 0;
    if (b!=0) {
      int c = a/b;
    } else {
      System.out.println("no way to div by zero");
    }
    // switch-case
    int num = 100;
    switch (num) {
      case 1000:
        System.out.println("THOUSAND");
        break;
      case 100:
        System.out.println("HUNDRED");;
        break;
      case 10:
        System.out.println("TEN");
        break;
      default: throw new IllegalArgumentException("not covered");
//        System.out.println("OTHER");
    }

    for (int i = 0; i < 5; i++) {
      System.out.println(i);
    }

    for (int i = 0;; i++) {
      if (!(i<5)) break;
      //
      //
      if (false) continue;
      //
      //
      //
      System.out.println(i);
    }

    for (int i = 0; i < 5;) {
      System.out.println(i);
      i++;
    }

    int i = 0;
    for (; i < 5; i++) {
      System.out.println(i);
    }
    System.out.println(i);

    for (int j = 0; j < 5; System.out.println(j), j++) {
    }

//    for (;;) {
//      System.out.println(".");
//    }

    int k = 0;
    while (k<5) {
      System.out.println(k);
      k++;
    }

    do {
      System.out.println(k);
      k++;
    } while (k<10);

    final int l = 777;
//    l = 999;

//    int h = k > 10 ? 1 : 2;
    int h;
    if (k>10) {
      h = 1;
    } else {
      h = 2;
    }





  }
}
