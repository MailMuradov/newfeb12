package lesson01;

import java.io.InputStream;
import java.util.Scanner;

public class ScannerApp {
  public static void main(String[] args) {
    // 3.
    System.out.println("Enter your name");
    // 1
    InputStream in = System.in;
    Scanner scanner = new Scanner(in);
    String name = scanner.nextLine();
    // 2
    String combined = String.format("Hello, %s\n", name);
    // 3
    System.out.print(combined);
  }
}
