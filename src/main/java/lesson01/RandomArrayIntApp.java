package lesson01;

import java.util.Arrays;

public class RandomArrayIntApp {
  public static void main(String[] args) {
    int[] a = new int[30];
    for (int i = 0; i < a.length; i++) {
      a[i] = (int) (Math.random()*200-100);
    }
    String aStringRep = Arrays.toString(a);
    System.out.println(aStringRep);
  }
}
