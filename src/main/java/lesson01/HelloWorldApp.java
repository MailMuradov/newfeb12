package lesson01;

import java.io.PrintStream;

public class HelloWorldApp {

  public static void main(String[] args) {
    PrintStream output = System.out;
    String line = "Hello World";
    String line2 = line.toUpperCase();
    output.println(line2);
  }

}
